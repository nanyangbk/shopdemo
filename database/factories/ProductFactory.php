<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Brand;

$factory->define(Product::class, function (Faker $faker) {
    $name = $faker->name;
    $slug = Str::slug($name);
    $price = $faker->numberBetween(100, 500);
    $sale_price = $price - $faker->numberBetween(10, 50);

    return [
        'name' => $name,
        'slug' => $slug,
        'price' => $price,
        'sale_price' => $sale_price,
        'description' => $faker->paragraph,
        'brand_id' => factory(Brand::class),
    ];
});
