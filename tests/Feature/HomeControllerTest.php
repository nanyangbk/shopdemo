<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Product;
use App\Brand;

class HomeControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * Test Add To Cart feature.
     *
     * @return void
     */
    public function testAddToCart()
    {
        $product = factory(Product::class)->create();
        $quantity = $this->faker->numberBetween(1, 10);

        $response = $this->call('POST', '/add-to-cart', ['productId' => $product->id, 'quantity' => $quantity]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'cart' => [
                    $product->id => [
                        'name' => $product->name,
                        'quantity' => $quantity,
                        'price' => $product->sale_price,
                    ],
                ]
            ])
            ->assertSessionHas('cart', [
                $product->id => [
                    'name' => $product->name,
                    'quantity' => $quantity,
                    'price' => $product->sale_price,
                ]
            ]);

        // Add one more time
        $newQuantity = $this->faker->numberBetween(1, 10);
        $response = $this->call('POST', '/add-to-cart', ['productId' => $product->id, 'quantity' => $newQuantity]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'cart' => [
                    $product->id => [
                        'name' => $product->name,
                        'quantity' => $quantity + $newQuantity,
                        'price' => $product->sale_price,
                    ],
                ]
            ])
            ->assertSessionHas('cart', [
                $product->id => [
                    'name' => $product->name,
                    'quantity' => $quantity + $newQuantity,
                    'price' => $product->sale_price,
                ]
            ]);
    }

    /**
     * Test Clear Cart feature.
     *
     * @return void
     */
    public function testClearCart()
    {
        $product = factory(Product::class)->create();
        $quantity = $this->faker->numberBetween(1, 10);

        $response = $this->withSession([
            'cart' => [
                $product->id => [
                    'name' => $product->name,
                    'quantity' => $quantity,
                    'price' => $product->sale_price,
                ],
            ]
        ])->call('POST', '/clear-cart');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
            ])
            ->assertSessionMissing('cart');
    }
}
