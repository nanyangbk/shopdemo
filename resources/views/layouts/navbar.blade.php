<div class="container navbar-header">
    <div class="row">
        <div class="col">
            <a href="/"><img src="/images/logo.png" width="150" alt="logo"></a>
        </div>
        <div class="col">
            <div class="float-right">
                <cart></cart>
            </div>
        </div>
    </div>
</div>
