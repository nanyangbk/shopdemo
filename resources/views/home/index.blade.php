@extends('layouts.app')

@section('title', 'Welcome to Shopdemo!')

@section('content')
    <div class="container">
        <h2 class="head-line">Home - Women's Top - Product Names</h2>
        @foreach ($products as $product)
            <product :product="{{ $product }}"></product>
        @endforeach
    </div>

@endsection
