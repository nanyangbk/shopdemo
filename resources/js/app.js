require('./bootstrap')

import Vue from 'vue'
import Vuex from 'vuex'
import Toasted from 'vue-toasted'

Vue.use(Vuex)
Vue.use(Toasted, {
    duration: 3000,
    type: 'success'
})

Vue.component('cart', require('./components/Cart.vue').default)
Vue.component('product', require('./components/Product.vue').default)

const store = new Vuex.Store({
    state: {
        cart: [],
    },
    mutations: {
        setCart (state, value) {
            state.cart = value
        },
    }
})

const app = new Vue({
    el: '#app',
    store: store
})
