# Deployment Guide

## Server Requirements:
Ubuntu

PHP >= 7.2

Nginx

Mysql 5.7

## Open terminal and type:

cd /var/www

git clone https://gitlab.com/nanyangbk/shopdemo.git

git config core.fileMode false

cp .env.example .env (then modify it)

composer install

php artisan key:generate

php artisan storage:link

sudo chgrp -R www-data storage bootstrap/cache public/storage

sudo chmod -R ug+rwx storage bootstrap/cache public/storage

## Database and sample data
- Create new database named "shopdemo"
- Import file database/shopdemo.sql
- Extract file database/sample_data.zip then upload these folders to the public/storage folder on the server

## Domain
- Register a free domain at dot.tk
- Add DNS Records:
    - A | 14440 | <your_server_ip_address>
    - WWW | CNAME | 14440 | <your_domain>
- Save changes

## Config Nginx

- Create file /etc/nginx/sites-available/<your_domain> with this content:

```
server {
    listen 80;
    listen [::]:80;

    root /var/www/shopdemo/public;
    index index.php index.html index.htm;

    charset utf-8;

    server_name <your_domain>  www.<your_domain>;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location = /public/favicon.ico { access_log off; log_not_found off; }
    location = /public/robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/<your_domain>-error.log error;
    sendfile off;

    client_max_body_size 100m;
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    location ~ /\.ht {
            deny all;
    }
}
```

Notes: Replace php7.3-fpm.sock with your server PHP version

- Create a soft link to sites-enabled directory by typing:

    `ln -s /etc/nginx/sites-available/<your_domain> /etc/nginx/sites-enabled/`

- Restart nginx: sudo service nginx restart

## Admin login

Account: admin@admin.com/password
