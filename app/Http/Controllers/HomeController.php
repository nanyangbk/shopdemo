<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public function index()
    {
        $topProducts = Cache::get('topProducts');
        if (!$topProducts) {
            $topProducts = Product::with('brand')->with('images')->take(Product::TOP_LIMIT)->get();
            Cache::put('topProducts', $topProducts, env('TOP_PRODUCTS_CACHE_LIFETIME', 600));
        }

        return view('home.index', ['products' => $topProducts]);
    }

    public function addToCart(Request $request)
    {
        try {
            $productId = $request->productId;
            $quantity = $request->quantity;
            $product = Product::find($productId);
            $cart = session()->get('cart');
            if (!$cart) {
                $cart = [];
            }
            if (isset($cart[$productId])) {
                $cart[$productId]['quantity'] = $cart[$productId]['quantity'] + $quantity;
            } else {
                $cart[$productId] = [
                    'name' => $product->name,
                    'quantity' => $quantity,
                    'price' => $product->sale_price ?: $product->price,
                ];
            }

            session()->put('cart', $cart);

            return response()->json(['status' => 'success', 'cart' => session()->get('cart')]);
        } catch (\Exception $ex) {
            return response()->json(['status' => 'error', 'message' => $ex->getMessage()]);
        }
    }

    public function getCart()
    {
        $cart = session()->get('cart');

        return response()->json(['cart' => $cart]);
    }

    public function clearCart()
    {
        try {
            session()->remove('cart');

            return response()->json(['status' => 'success']);
        } catch (\Exception $ex) {
            return response()->json(['status' => 'error']);
        }
    }
}
