<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class ProductImage extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = ['id', 'deleted_at'];
    protected $appends = ['thumbnails'];

    public static function boot()
    {
        parent::boot();

        static::updating(function($model)
        {
            // Check if Old File Exists
            $oldFileExists = Storage::disk('public')->exists($model->original['filename']);

            if($oldFileExists && $model->filename != $model->original['filename'])
            {
                Storage::disk('public')->delete($model->original['filename']);
                Storage::disk('public')->delete(self::thumbnails($model->original['filename']));
            }
        });
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public static function thumbnails($filename)
    {
        if ($filename) {
            return dirname($filename) . '/thumbnails/' . basename($filename);
        }

        return null;
    }

    public function getThumbnailsAttribute($value)
    {
        return self::thumbnails($this->filename);
    }
}
